import random
import math

class Neuron:
	def __init__(self, activationFunction):
		self.activationFunction = activationFunction
		self.weights = []

	def generateRandomWeights(self, sizeLayer, minWeight=0.1, maxWeight=1.0):
		#print("num weights: {}".format(sizeLayer))
		for neuron in range(sizeLayer):
			distance = maxWeight - minWeight
			weight = (random.random() * distance) + minWeight 
			self.weights.append(weight)

	def calculateActivation(self,prevLayerOutput):
		weightedSum = 0

		#print(str(len(prevLayerOutput)))

		for prevNeuronId in range(len(prevLayerOutput)):
			weightedSum += self.weights[prevNeuronId] * prevLayerOutput[prevNeuronId]

		return weightedSum

	def calculateOutput(self, activation):
		return self.activationFunction.function(activation)

	def calc(self, prevLayerOutput):
		activation = self.calculateActivation(prevLayerOutput)
		return self.calculateOutput(activation)

def calculateError(expected, actual):
	difference = 0.0
	for outputId in range(len(expected)):
		difference += pow(expected[outputId] - actual[outputId],2)
	return  pow(difference, 0.5) 

class Network:
	def __init__(self, inputs, outputs):
		self.layers = []

		self.inputs = inputs
		self.outputs = outputs

	def generateLayers(self, layers, neuronsPerLayer, activationFunction):
			for layer in range(1, layers + 1):
				neurons = []

				if layer == layers:
					numNeuronsLayer = self.outputs
				else:
					numNeuronsLayer = neuronsPerLayer
				
				for neuron in range(numNeuronsLayer):
					neurons.append(Neuron(activationFunction))
	
				self.layers.append(neurons)

	def initRandomWeights(self, min=0.1, max=1.0):
		for layerId in range(len(self.layers)):
			sizePrevLayer = self.inputs if (layerId == 0) else len(self.layers[layerId-1])
			for neuron in self.layers[layerId]:
				neuron.generateRandomWeights(sizePrevLayer, min, max)

	def calculateOutput(self, inp):
		output = inp
		for layer in range(len(self.layers)):
			output = self.calculateLayer(layer, output)
		return output		

	def calculateLayer(self, layerId, inp):
		outputLayer = []

		#print("calculating layer with len:{}".format(len(self.layers[layerId])))

		for neuron in self.layers[layerId]:
			outputNeuron = neuron.calc(inp)
			#print(outputNeuron)
			outputLayer.append(outputNeuron)
		return outputLayer

	def calculateDifference(self, expected, actual):
		difference = []
		for outputId in range(len(expected)):
			difference.append(expected[outputId] - actual[outputId])
		return difference

	@staticmethod
	def trainOutputLayer(layer, outputsPrev, activation, learningRate, diffValues):
		deltas = []

		#print(activation)
		#print(diffValues)
		                                                                   

		for neuronId in range(len(layer)):
			neuron = layer[neuronId]

			delta = neuron.activationFunction.derivation(activation[neuronId]) * diffValues[neuronId]
			deltas.append(delta)
			
			neuron = layer[neuronId]
			weights = neuron.weights
			for neuronIdPrev in range(len(weights)): 
				changeWeight = learningRate * outputsPrev[neuronIdPrev] * delta
				weights[neuronIdPrev] += changeWeight
		
		return deltas

	@staticmethod
	def trainHiddenLayer(layer, nextLayer, outputsPrev, activation, deltasNext, learningRate):
		deltas = []
		for neuronId in range(len(layer)):
			neuron = layer[neuronId]

			weightedSumDeltas = 0.0
			for neuronIdNext in range(len(nextLayer)):
				weight = nextLayer[neuronIdNext].weights[neuronId]

				weightedSumDeltas += deltasNext[neuronIdNext] * weight

			delta = neuron.activationFunction.derivation(activation[neuronId]) * weightedSumDeltas
			deltas.append(delta)
			
			for neuronIdPrev in range(len(neuron.weights)): 
				changeWeight = learningRate * outputsPrev[neuronIdPrev] * delta
				neuron.weights[neuronIdPrev] += changeWeight
		
		return deltas

	def trainNetwork(self, inp, expected, learningRate):
		trainingsData = []
		
		output = inp
		for layer in range(len(self.layers)):
			outputDict = self.calculateTrainingDataLayer(layer, output)
			output = outputDict['output']

			trainingsData.append(outputDict)

		#train output layer
		output = trainingsData[-1]['output']
		diffValues = self.calculateDifference(expected, output)

		outputPrev = trainingsData[-2]['output']
		activationCur = trainingsData[-1]['activation']

		layer = (self.layers[-1])
		deltas = Network.trainOutputLayer(layer, outputPrev, activationCur, learningRate, diffValues)

		#train hidden layer
		for trainingsLayerId in reversed(range(len(self.layers)-1)):
			if trainingsLayerId == 0:
				dataPrev = inp
			else :
				dataPrev = trainingsData[trainingsLayerId]['output']
			
			activationCur = trainingsData[trainingsLayerId]['activation']

			layer = self.layers[trainingsLayerId]	
			nextLayer = self.layers[trainingsLayerId + 1]
			deltas = Network.trainHiddenLayer(layer, nextLayer, outputPrev, activationCur, deltas, learningRate)

		return trainingsData[-1]['output']


	def calculateTrainingDataLayer(self, layerId, inp):
		activationLayer = []
		outputLayer = []
		for neuron in self.layers[layerId]:
			activationNeuron = neuron.calculateActivation(inp)
			outputNeuron = neuron.calculateOutput(activationNeuron)

			activationLayer.append(activationNeuron)
			outputLayer.append(outputNeuron)
		return { 'activation' : activationLayer, 'output' : outputLayer }

	def printNetwork(self):
		print("Printing NN:\n")
		layerCount = 0
		for layer in self.layers:
			print("\tprinting Layer {}".format(layerCount))
			for neuron in layer:
				neuronStr = "\t\t"
				for weight in neuron.weights:
					neuronStr += "|{}".format(str(weight))
				print(neuronStr + "|")
			layerCount += 1


class SimpleActivationFunction:
	@staticmethod
	def function(activation):
		return 1.0 / ( 1 + pow(math.e,(activation * 1) ) ) 

	@staticmethod
	def derivation(activation):
		base = SimpleActivationFunction.function(activation)
		return  base * (1.0 - base)

def generateTestDataSet(numSamples, maxValue = 2.0):
	samples = []
	
	for sampleId in range(numSamples):
		input = [(random.random() *  maxValue) - (maxValue / 2.0), (random.random() *  maxValue) - (maxValue / 2.0)] 
		output = [ 1.0 if input[0] >= 0.0 else -1.0 ] #[input[0] + input[1]]#
		samples.append({ 'input' : input, 'output' : output })
	
	return samples

def printTestCase(testCase):
	print('TestCase')
	print(testCase['input'])
	print(testCase['output'])


network = Network(2, 1)
network.generateLayers(2, 2, SimpleActivationFunction)
network.initRandomWeights(0.2, 0.8)
network.printNetwork()

input = [1.0, 1.0]
output =  network.calculateOutput(input)

print(input)
print(output)

testData = generateTestDataSet(500, 30)

for testCase in testData:
	printTestCase(testCase) 

iteration = 0
error = 10.0

repetitions = 10

while (error >= 0.2):
	for testCase in testData:
		for repetition in range(repetitions):
			network.trainNetwork(testCase['input'], testCase['output'], 0.01)

	erorSum = 0.0
	for testCase in testData:
		output = network.calculateOutput(testCase['input'])

		#print(output)

		activeError = calculateError(output,testCase['output'])
		
		#print(activeError)

		erorSum += activeError


	#print(erorSum)

	error = erorSum / len(testData) 
	iteration += 1

	

	print("Completed Training iteration {} with an error of {}".format(iteration, error))